**Current Features**

* The design includes classes with clear separation of concerns. AWS S3, AWS DynamoDB, VimeoClient, MySqlClient
* The runner is glue that choreographs the entire workflow.
* The Rate limit optimization is done with following consideration
    * Each page return 25 results as per Vimeo API documentation.
    
    * Requirement is to fetch 3 results (which means 3 API calls)
    
    * We rather made 1 API call with 25 * 3 = 75 results. This will help us make less calls and yet get the same data back from Vimeo.
    
* The interactions with S3 are done in batch mode. For every movie, we make 1 batch insert into S3 and DynamoDB. This will help us save on latency.

**To Run**

```
git clone git@bitbucket.org:deeksha/parrotanalyticstest.git
cd panalytics
mvn clean package
java -cp .:target/parrot-1.0-SNAPSHOT.jar runner.Main

```



**Enhancements**  
There are following enhancements that can be taken up next:  
Externalizing all the properties/access keys/database names. These will then be passed via command-line argument. This provides multiple benefits

   Change in the properties will not cause re-compilation/bundling/re-deployment of application.
     
   Externalizing avoids coupling that we introduce with the environment. With this same code can run on local/dev/qa/staging and prod environments
   
   The properties may have sensitive data (ex. passwords/access keys) that we should not want to check in for production code.
    
* Provide validation on inputs. Example: how many pages we may want to fetch from vimeo

* Better exception handling: Currently the application logs and continues. Ideally, we may want to take preventive actions involving rollback of the data if needed. These could be made part of this program

* Intelligent rate limit algorithm: The vimeo api has generous search limits, but is sends the header in response to how many remaining calls we can make. Currently, we are not using this information (because of optimization introduced), but using this header will make our program more robust.
