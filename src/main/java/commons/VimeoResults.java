package commons;


/**
 * Created by deeksha on 6/04/17.
 */
public class VimeoResults {
    private long totalViews;
    private String rawResults;

    public VimeoResults(long totalViews, String rawResults) {
        this.totalViews = totalViews;
        this.rawResults = rawResults;
    }

    public long getTotalViews() {
        return totalViews;
    }

    public String getRawResults() {
        return rawResults;
    }
}
