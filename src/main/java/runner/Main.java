package runner;

import api.AwsDynamoConnector;
import api.AwsS3Connector;
import api.VimeoConnector;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.dynamodbv2.document.Item;
import commons.VimeoResults;
import db.DbClient;

import java.sql.SQLException;
import java.util.Collections;
import java.util.Map;

/**
 * Created by deeksha on 6/04/17.
 */
public class Main {

    //TODO Externalize/env variable
    private static final int NUM_PAGES = 3;

    public static void main(String[] args) throws SQLException {
        DbClient dbClient = new DbClient();
        Map<String, String> cache = dbClient.getMovieSearchTerms();
        AWSCredentials credentials = getCredentials();

        cache.entrySet()
                .parallelStream()
                .forEach(entry -> {
                    VimeoResults results = VimeoConnector.fetchVimeoResult(NUM_PAGES, entry.getValue());
                    String currentTimestamp = String.valueOf(System.currentTimeMillis());

                    AwsS3Connector.uploadToS3(results.getRawResults(), credentials,currentTimestamp);
                    Item item = generateItemFrom(results.getTotalViews(), entry.getKey(),currentTimestamp);
                    AwsDynamoConnector.storeInDb(Collections.singletonList(item),credentials);
                });
        System.out.println("Data Extracted!");
    }

    private static Item generateItemFrom(long totalViews,String movieName, String timestamp){
        return new Item()
                .withPrimaryKey("title", String.valueOf(totalViews))
                .withString("movie",movieName)
                .withString("timestamp", timestamp);
    }

    private static AWSCredentials getCredentials(){
        return new BasicAWSCredentials(
                "AKIAITPZUM6PGX4FE24Q",
                "+CtFSl1mdStmTlq7mmREeUbVLfzp75PcH3tOBUM9"
        );
    }
}
