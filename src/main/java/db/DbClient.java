package db;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by deeksha on 6/04/17.
 */
public class DbClient {


    String url = "jdbc:mysql://technicaltest.ctkeuweqhlpx.us-east-1.rds.amazonaws.com:3306/";
    String userName = "techtest";
    String password = "playp3n";
    String dbName = "movies";
    String table = "movie_search";
    Connection connection;

    public DbClient() throws SQLException {
        connection =DriverManager.getConnection(url+dbName+"?useSSL=false",userName,password);
    }

    public Map<String, String> getMovieSearchTerms(){
        Map<String, String> cache = new HashMap<String, String>();
        Statement st = null;
        try {
            st = connection.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String queryString = "SELECT * FROM "+table;
        ResultSet resultSet = null;
        try {
            resultSet =st.executeQuery(queryString);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            while (resultSet.next()){
                cache.put(resultSet.getString("movie_title"), resultSet.getString("search_term"));
            }
            resultSet.close();
            st.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
                try {
                    if(st != null)
                    st.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }
        return cache;
    }


}
