package api;


import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.BatchWriteItemOutcome;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.TableWriteItems;

import java.util.List;

/**
 * Created by deeksha on 6/04/17.
 */
public class AwsDynamoConnector {
    private static final String TABLE = "ddb-table-sst67gy";

    public static void storeInDb(List<Item> items, AWSCredentials credentials){
        DynamoDB dynamoDB = new DynamoDB(new AmazonDynamoDBClient(credentials));

        TableWriteItems movieItems = new TableWriteItems(TABLE).withItemsToPut(items);
        BatchWriteItemOutcome outcome = dynamoDB.batchWriteItem(movieItems);
        System.out.println(outcome);
    }
}
