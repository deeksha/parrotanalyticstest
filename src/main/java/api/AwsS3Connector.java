package api;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;

/**
 * Created by deeksha on 6/04/17.
 */
public class AwsS3Connector {

    private static String bucketName     = "s3bucket-sst67gy";
    private static String keyName        = "viewcount.json";

    /**
     *
     * @param content
     */
    public static void uploadToS3(String content, AWSCredentials credentials, String timestamp){
        AmazonS3 s3Client = new AmazonS3Client(credentials);
        try {
            System.out.println("Uploading a new object to S3 from a file\n");
            byte[] contentAsBytes = content.getBytes("UTF-8");
            ByteArrayInputStream contentsAsStream      = new ByteArrayInputStream(contentAsBytes);
            ObjectMetadata md = new ObjectMetadata();
            md.setContentLength(contentAsBytes.length);
            s3Client.putObject(new PutObjectRequest(bucketName, timestamp+"-"+keyName, contentsAsStream, md));
        } catch (AmazonServiceException ase) {
            System.out.println("Caught an AmazonServiceException, which " +
                    "means your request made it " +
                    "to Amazon S3, but was rejected with an error response" +
                    " for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            System.out.println("Error Message: " + ace.getMessage());
        } catch (UnsupportedEncodingException e) {
            System.out.println("Content cannot be encoded to UTF-8");
            e.printStackTrace();
        }
    }
}

