package api;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonValue;
import commons.VimeoResults;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

/**
 * Created by deeksha on 6/04/17.
 */
public class VimeoConnector {

    // TODO: Externalize the properties while refactoring the code later
    private static final String ACCESS_TOKEN = "Bearer f0f6514ca1bbe2272a32389f8b58d39c";
    private static final String VIDEO_URL = "https://api.vimeo.com/videos";

    private static String getSearchResults(int pageNumber, String queryTerm) {
        final String url = VIDEO_URL + "?page" + String.valueOf(pageNumber) + "&query=" + queryTerm;
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(url);
        Response response = target.request().header(HttpHeaders.AUTHORIZATION,  ACCESS_TOKEN).get();
        String value = response.readEntity(String.class);
        response.close();
        return value;
    }

    private static long getTotalViewsPerPage(JsonValue jsonResponse) {
        JsonArray data = jsonResponse.asObject().get("data").asArray();
        long totalViews = 0l;

        for (JsonValue jsonValue : data) {
            try {
                totalViews += jsonValue.asObject().get("stats").asObject().get("plays").asLong();
            } catch (UnsupportedOperationException e) {
                System.out.println("No totalViews for " + jsonValue.asObject().get("stats").asObject());
            }
        }
        return totalViews;
    }

    public static VimeoResults fetchVimeoResult(int numberOfPages, String queryTerm) {
        long totalViewCountForSearchTerm = 0l;
        JsonArray rawResultsForS3 = new JsonArray();

        for(int page = 1; page <= numberOfPages; page ++) {
            System.out.println("----------- Page " + page + " ---------------");

            String jsonResponseBody = getSearchResults(page, queryTerm);
            JsonValue response = Json.parse(jsonResponseBody);

            long viewsOnPage = getTotalViewsPerPage(response);

            System.out.println("results on page : " + viewsOnPage);
            System.out.println("---------- End of Page " + page + " ----------");

            totalViewCountForSearchTerm += viewsOnPage;
            rawResultsForS3.add(response);
        }
        System.out.println("Total records fetched: " + totalViewCountForSearchTerm);
        System.out.println("Raw responses fetched: " + rawResultsForS3.size());
        System.out.println(rawResultsForS3.toString());
        return new VimeoResults(totalViewCountForSearchTerm, rawResultsForS3.toString());
    }
}
